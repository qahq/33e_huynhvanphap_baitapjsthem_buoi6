function handleKetQua(){
    var soNguyenTo = document.getElementById("txt-so-nguyen-to").value * 1;
    var inKetQua = "";
    for (var i = 2 ; i <=soNguyenTo; i++){
        var test = testNumber(i);
        if(test){
            inKetQua += i + " ";
        }
    }
    document.getElementById("result").innerHTML=`<h2 class="text-secondary"> Số Nguyên Tố : ${inKetQua}</h2>`
}

function testNumber(number){
    var test = true;
    for(var i = 2; i <= Math.sqrt(number); i++){
        if (number % i ==0){
            test = false;   
            break;
        }
    }
    return test;
}